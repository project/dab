<?php

namespace Drupal\dab\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\dab\Traits\DabComponentTrait;

/**
 * Access check to avoid crud on core and contrib components.
 */
class CrudComponentAccessCheck implements AccessInterface {

  use StringTranslationTrait;
  use DabComponentTrait;

  /**
   * Constructs a new CrudComponentAccessCheck object.
   */
  public function __construct(private CurrentRouteMatch $routeMatch) {
    $this->routeMatch = $routeMatch;
  }

  /**
   * Checks access for a specific DAB component route.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account object.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(AccountInterface $account): AccessResult {
    $provider = $this->routeMatch->getParameter('provider');
    $machineName = $this->routeMatch->getParameter('machine_name');

    // If the provider or machine name is not set, allow access.
    // It means this is a new component.
    if (is_null($provider) || is_null($machineName)) {
      return AccessResult::allowed();
    }

    // Get the component data.
    $this->getComponentData($machineName, $provider);
    $componentPluginDefinition = $this->component->getPluginDefinition();

    // Ensure the user has the required permission.
    if (!$account->hasPermission('administer dab components')) {
      return AccessResult::forbidden()->addCacheContexts(['user.permissions']);
    }

    // Check if the component is not custom (Core or Contrib paths).
    if (preg_match(
      '/((core)+\/(modules|themes))|((modules|themes)\/(contrib)+)/',
      $componentPluginDefinition['path']
    )) {
      return AccessResult::forbidden();
    }

    // If all checks pass, allow access.
    return AccessResult::allowed();
  }

}
