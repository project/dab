<?php

namespace Drupal\dab\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Theme\ComponentPluginManager;
use Drupal\Core\Url;
use Drupal\dab\Form\ComponentFilterForm;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines DabComponentListController class.
 */
final class DabComponentListController implements ContainerInjectionInterface {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * The component plugin manager.
   *
   * @var \Drupal\Core\Theme\ComponentPluginManager
   */
  protected ComponentPluginManager $componentPluginManager;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected FormBuilderInterface $formBuilder;

  /**
   * The current stack.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $currentRequest;

  /**
   * Constructs a new Drupal\dab\Controller\DabComponentListController object.
   */
  public function __construct(
    ComponentPluginManager $componentPluginManager,
    FormBuilderInterface $formBuilder,
    RequestStack $requestStack,
  ) {
    $this->componentPluginManager = $componentPluginManager;
    $this->formBuilder = $formBuilder;
    $this->currentRequest = $requestStack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.sdc'),
      $container->get('form_builder'),
      $container->get('request_stack'),
    );
  }

  /**
   * Returns the title for the component list.
   *
   * @param string|null $component_type
   *   The component type.
   *
   * @return string
   *   The title.
   */
  public function getTitle($component_type = NULL) {
    return is_null($component_type)
      ? 'Drupal Atomic Builder'
      : $this->t(
        '@component_type',
        ['@component_type' => ucfirst($component_type)]
      );
  }

  /**
   * Check if the component is excluded from the list.
   *
   * @param string $group
   *   The group name.
   * @param string $componentName
   *   The component name.
   * @param string $extensionValue
   *   The extension value.
   * @param string $origin
   *   The origin of the component.
   *
   * @return bool
   *   Return true if the component is excluded.
   */
  private function isComponentExcluded(
    string $group,
    string $componentName,
    string $extensionValue,
    string $origin,
  ): bool {
    $currentGroup = $this->currentRequest->attributes->get('component_type') ?? '';
    $filter = $this->currentRequest->query->get('filter') ?? '';
    $extensionFilter = $this->currentRequest->query->get('extension_type') ?? '';
    $originFilter = $this->currentRequest->query->get('origin') ?? '';

    $isGroupFiltered = !empty($currentGroup) && $currentGroup !== $group;
    $isFiltered = !empty($filter) && strpos($componentName, $filter) === FALSE;
    $isExtensionFiltered = !empty($extensionFilter) && $extensionValue !== $extensionFilter;
    $isOriginFiltered = !empty($originFilter) && $origin !== $originFilter;

    return $isGroupFiltered || $isFiltered || $isExtensionFiltered || $isOriginFiltered;
  }

  /**
   * Build the page data.
   *
   * @return array
   *   Return template and data.
   */
  public function build($component_type = NULL) {
    $componentList = [];
    $allComponents = [];

    try {
      $components = $this->componentPluginManager->getAllComponents();
    }
    catch (\Exception $e) {
      $this->messenger()->addError($e->getMessage());
      return [];
    }

    /** @var \Drupal\Core\Plugin\Component $component */
    foreach ($components as $component) {
      $pluginDefinition = $component->getPluginDefinition();
      $componentName = $pluginDefinition['machineName'];
      $pluginId = $component->getPluginId();
      $group = $pluginDefinition['group'] ?? 'other';
      // Sanitize the group name.
      $group = Html::getId($group);
      $extension = $pluginDefinition['extension_type'] ?? '';
      $origin = match (TRUE) {
        (bool) preg_match('/(modules|themes)\/custom/', $pluginDefinition['path']) => 'custom',
        (bool) preg_match('/(modules|themes)\/contrib/', $pluginDefinition['path']) => 'contrib',
        (bool) preg_match('/core\/(modules|themes)/', $pluginDefinition['path']) => 'core',
        default => '',
      };
      $urlQuery = $this->currentRequest->query->all();

      if (!isset($componentList[$group])) {
        $componentList[$group] = [
          'title' => ucfirst($group),
          'url' => Url::fromRoute(
            'dab.component_type_list',
            ['component_type' => $group],
            ['query' => $urlQuery]
          ),
          'class' => [
            ...($component_type === $group ? ['active'] : []),
          ],
        ];
      }

      if ($this->isComponentExcluded($group, $componentName, $extension->value, $origin)) {
        continue;
      }

      $componentList[$group]['components'][$pluginId] = [
        'title' => ($pluginDefinition['name'] ?? ucfirst($componentName)),
        'machine_name' => $pluginId,
        'description' => $pluginDefinition['description'] ?? '',
        'extension' => $pluginDefinition['extension_type'] ?? '',
        'origin' => $origin,
        'url' => Url::fromRoute(
          'dab.component',
          [
            'component_type' => $group,
            'machine_name' => $componentName,
            'provider' => $pluginDefinition['provider'],
          ],
          ['query' => $urlQuery]
        ),
      ];

      $allComponents[$pluginId] = $componentList[$group]['components'][$pluginId];
    }

    ksort($componentList);

    return [
      '#theme' => 'dab_component_list',
      '#components_types' => $componentList,
      '#components' => empty($component_type) || empty($componentList[$component_type]['components']) ? $allComponents : $componentList[$component_type]['components'],
      '#form' => $this->formBuilder->getForm(ComponentFilterForm::class),
      '#attached' => [
        'library' => [
          'dab/global',
        ],
      ],
    ];
  }

}
