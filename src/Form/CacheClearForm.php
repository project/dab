<?php

namespace Drupal\dab\Form;

use Drupal\Core\Asset\AssetQueryStringInterface;
use Drupal\Core\Asset\CssCollectionOptimizerLazy;
use Drupal\Core\Asset\JsCollectionOptimizerLazy;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\TwigEnvironment;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a cache clearing form.
 */
final class CacheClearForm extends FormBase implements ContainerInjectionInterface {

  /**
   * The js collection optimizer.
   *
   * @var \Drupal\Core\Asset\JsCollectionOptimizerLazy
   */
  protected JsCollectionOptimizerLazy $jsCollectionOptimizer;

  /**
   * The css collection optimizer.
   *
   * @var \Drupal\Core\Asset\CssCollectionOptimizerLazy
   */
  protected CssCollectionOptimizerLazy $cssCollectionOptimizer;

  /**
   * The asset query string.
   *
   * @var \Drupal\Core\Asset\AssetQueryStringInterface
   */
  protected AssetQueryStringInterface $assetQueryString;

  /**
   * The twig service.
   *
   * @var \Drupal\Core\Template\TwigEnvironment
   */
  protected $twig;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cache_clear_form';
  }

  /**
   * Constructs a new Drupal\dab\Form\CacheClearForm object.
   */
  public function __construct(
    JsCollectionOptimizerLazy $js_collection_optimizer,
    CssCollectionOptimizerLazy $css_collection_optimizer,
    AssetQueryStringInterface $asset_query_string,
    TwigEnvironment $twig,
  ) {
    $this->jsCollectionOptimizer = $js_collection_optimizer;
    $this->cssCollectionOptimizer = $css_collection_optimizer;
    $this->assetQueryString = $asset_query_string;
    $this->twig = $twig;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('asset.js.collection_optimizer'),
      $container->get('asset.css.collection_optimizer'),
      $container->get('asset.query_string'),
      $container->get('twig'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['cache_clear'] = [
      '#type' => 'submit',
      '#id' => 'cache-reload-button',
      '#value' => '↻',
      '#button_type' => 'primary',
      '#attributes' => [
        'title' => $this->t('Reload the page and clear all cache on click or keyboard shortcut'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Flush asset file caches.
    $this->jsCollectionOptimizer->deleteAll();
    $this->cssCollectionOptimizer->deleteAll();
    $this->assetQueryString->reset();

    // Wipe the Twig PHP Storage cache.
    $this->twig->invalidate();
  }

}
