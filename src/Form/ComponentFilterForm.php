<?php

namespace Drupal\dab\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * A simple form to filter components in DabComponentListController.
 */
final class ComponentFilterForm extends FormBase implements ContainerInjectionInterface {

  /**
   * The current stack.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private $currentRequest;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new Drupal\dab\Form\ComponentFilterForm object.
   */
  public function __construct(
    RequestStack $requestStack,
    RouteMatchInterface $routeMatch,
  ) {
    $this->currentRequest = $requestStack->getCurrentRequest();
    $this->routeMatch = $routeMatch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'component_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];

    $form['#attributes'] = [
      'class' => ['dab-form-filter'],
    ];

    $form['filter-container'] = [
      '#type' => 'fieldset',
      '#attributes' => [
        'class' => ['dab-form-filter__container'],
      ],
    ];

    $form['filter-container']['filter'] = [
      '#type' => 'search',
      '#title' => $this->t('Filter by component'),
      '#maxlength' => 64,
      '#size' => 24,
      '#default_value' => $this->currentRequest->query->get('filter') ?? '',
      '#ajax' => [
        'callback' => '::clearFilter',
        'event' => 'search',
        'progress' => [],
      ],
    ];

    $form['filter-container']['extension_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Filter by extension type'),
      '#options' => [
        '' => $this->t('All'),
        'module' => $this->t('Module'),
        'theme' => $this->t('Theme'),
      ],
      '#default_value' => $this->currentRequest->query->get('extension_type') ?? '',
    ];

    $form['filter-container']['origin'] = [
      '#type' => 'select',
      '#title' => $this->t('Filter by origin'),
      '#options' => [
        '' => $this->t('All'),
        'custom' => $this->t('Custom'),
        'contrib' => $this->t('Contrib'),
        'core' => $this->t('Core'),
      ],
      '#default_value' => $this->currentRequest->query->get('origin') ?? '',
    ];

    $form['filter-container']['actions'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['dab-form-filter__actions'],
      ],
    ];

    $form['filter-container']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
      '#button_type' => 'primary',
      '#attributes' => [
        'title' => $this->t('Filter'),
        'class' => ['dab-form-filter__submit'],
      ],
    ];

    if (!empty($this->currentRequest->query->all())) {
      $form['filter-container']['actions']['reset'] = [
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
        '#button_type' => 'secondary',
        '#attributes' => [
          'title' => $this->t('Reset'),
          'class' => ['dab-form-filter__reset'],
        ],
        '#id' => 'reset-filter-button',
      ];
    }

    return $form;
  }

  /**
   * Build the route parameters for the filtered form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param bool $clear
   *   Whether to clear the filter.
   *
   * @return array
   *   The route parameters.
   */
  private function buildRouteParameters(FormStateInterface $form_state, bool $clear = FALSE): array {
    $userInput = $form_state->getUserInput();
    $filter = $userInput['filter'] ?? '';
    $extensionTypeFilter = $userInput['extension_type'] ?? '';
    $originFilter = $userInput['origin'] ?? '';

    $routeParameters = [
      'component_type' => $this->routeMatch->getParameter('component_type'),
    ];

    if (!$clear) {
      $routeParameters['filter'] = $filter;
      $routeParameters['extension_type'] = $extensionTypeFilter;
      $routeParameters['origin'] = $originFilter;
    }

    return $routeParameters;
  }

  /**
   * Clear the filter.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function clearFilter(array &$form, FormStateInterface $form_state) {
    $url = Url::fromRoute($this->routeMatch->getRouteName(), $this->buildRouteParameters($form_state));
    $response = new AjaxResponse();
    $response->addCommand(new RedirectCommand($url->toString()));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $form_state->setRedirect(
      $this->routeMatch->getRouteName(),
      $this->buildRouteParameters(
        $form_state,
        $trigger['#id'] === 'reset-filter-button'
      )
    );
  }

}
