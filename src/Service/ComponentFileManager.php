<?php

namespace Drupal\dab\Service;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\Component;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\dab\Form\ConfigureComponentsTypesForm;

/**
 * A class to create files for a new component.
 *
 * @package Drupal\dab\Service
 */
class ComponentFileManager {

  use StringTranslationTrait;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private FileSystemInterface $fileSystem;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private ConfigFactoryInterface $configFactory;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  private MessengerInterface $messenger;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  private ModuleExtensionList $moduleExtensionList;

  /**
   * The theme extension list.
   *
   * @var \Drupal\Core\Extension\ThemeExtensionList
   */
  private ThemeExtensionList $themeExtensionList;

  /**
   * Constructs a new Drupal\dab\Form\ComponentFileManager object.
   */
  public function __construct(
    FileSystemInterface $file_system,
    ConfigFactoryInterface $config_factory,
    MessengerInterface $messenger,
    ModuleExtensionList $module_extension_list,
    ThemeExtensionList $theme_extension_list,
  ) {
    $this->fileSystem = $file_system;
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->moduleExtensionList = $module_extension_list;
    $this->themeExtensionList = $theme_extension_list;
  }

  /**
   * Get the dab module path.
   *
   * @return string
   *   The dab module path.
   */
  public function getDabModulePath(): string {
    return $this->moduleExtensionList->getPath('dab');
  }

  /**
   * Build the component folder path.
   *
   * @param string $machineName
   *   The component machine name.
   * @param string $provider
   *   The component provider.
   * @param string|null $componentType
   *   The component type.
   *
   * @return string
   *   The component folder path.
   */
  public function buildComponentFolderPath(
    string $machineName,
    string $provider,
    ?string $componentType = NULL,
  ): string {
    $themeOrModulePath = $provider;

    // Get the theme of module path.
    if ($this->themeExtensionList->exists($provider)) {
      $themeOrModulePath = $this->themeExtensionList->getPath($provider);
    }

    if ($this->moduleExtensionList->exists($provider)) {
      $themeOrModulePath = $this->moduleExtensionList->getPath($provider);
    }

    $componentTypePath = !empty($componentType) ? "$componentType/" : '';
    return "$themeOrModulePath/components/$componentTypePath$machineName";
  }

  /**
   * Create the component folder.
   *
   * @param string $machineName
   *   The component machine name.
   * @param string $provider
   *   The component provider.
   * @param string|null $componentType
   *   The component type.
   *
   * @return bool
   *   TRUE if the folder was created.
   */
  public function createComponentFolder(
    string $machineName,
    string $provider,
    ?string $componentType = NULL,
  ): bool {
    $path = $this->buildComponentFolderPath($machineName, $provider, $componentType);
    return $this->fileSystem->prepareDirectory(
      $path,
      FileSystemInterface:: CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS
    );
  }

  /**
   * Create the component yaml file.
   *
   * @param string $machineName
   *   The component machine name.
   * @param string $provider
   *   The component provider.
   * @param string $name
   *   The component name.
   * @param string|null $componentType
   *   The component type.
   * @param string|null $description
   *   The component description.
   *
   * @return bool
   *   TRUE if the file was created.
   */
  public function createComponentFile(
    string $machineName,
    string $provider,
    string $name,
    ?string $componentType,
    ?string $description = NULL,
  ): bool {
    $file = fopen(
      $this->buildComponentFolderPath($machineName, $provider, $componentType) . "/$machineName.component.yml",
      'x'
    );

    if (!$file) {
      return FALSE;
    }

    $yamlArray = [
      '# Documentation' => 'https://www.drupal.org/docs/develop/theming-drupal/using-single-directory-components/annotated-example-componentyml',
      'name' => $name,
      'status' => 'experimental',
    ];

    if (!empty($componentType)) {
      $yamlArray['group'] = $componentType;
    }

    if (!empty($description)) {
      $yamlArray['description'] = $description;
    }

    $yamlArray['props'] = [
      'type' => 'object',
      'properties' => [
        'name' => [
          'type' => 'string',
          'examples' => [
            'Example 1' => 'Hello world',
          ],
        ],
      ],
    ];

    $yaml = Yaml::encode($yamlArray);
    fwrite($file, $yaml);
    return fclose($file);
  }

  /**
   * Create the component twig file.
   *
   * @param string $machineName
   *   The component machine name.
   * @param string $provider
   *   The component provider.
   * @param string|null $componentType
   *   The component type.
   *
   * @return bool
   *   TRUE if the file was created.
   */
  public function createTwigFile(
    string $machineName,
    string $provider,
    ?string $componentType,
  ): bool {
    $file = fopen(
      $this->buildComponentFolderPath($machineName, $provider, $componentType) . "/$machineName.twig",
      'x'
    );

    if (!$file) {
      return FALSE;
    }

    fwrite($file, "{# @file\n");
    fwrite($file, "  @component: $machineName\n");
    fwrite($file, "  @props:\n");
    fwrite($file, "    - name:\n");
    fwrite($file, "      type: string\n");
    fwrite($file, "#}\n");
    fwrite($file, "{{ name }}");
    return fclose($file);
  }

  /**
   * Create the component readme file.
   *
   * @param string $machineName
   *   The component machine name.
   * @param string $provider
   *   The component provider.
   * @param string $name
   *   The component name.
   * @param string $description
   *   The component description.
   * @param string $componentType
   *   The component type.
   *
   * @return bool
   *   TRUE if the file was created.
   */
  public function createReadmeFile(
    string $machineName,
    string $provider,
    string $name,
    string $description,
    ?string $componentType,
  ): bool {
    $file = fopen(
      $this->buildComponentFolderPath($machineName, $provider, $componentType) . "/README.md",
      'x'
    );

    if (!$file) {
      return FALSE;
    }

    fwrite($file, "# $name \n");
    fwrite($file, "\n");
    fwrite($file, "$description\n");
    fwrite($file, "# Usage \n");
    fwrite($file, "\n");
    fwrite($file, "Describe the usage of your component here. \n");
    fwrite($file, "# Additional Info\n");
    fwrite($file, "\n");
    fwrite($file, "Add additional info if needed here. \n");
    return fclose($file);
  }

  /**
   * Create the component js file.
   *
   * @param string $machineName
   *   The component machine name.
   * @param string $provider
   *   The component provider.
   * @param string|null $componentType
   *   The component type.
   *
   * @return bool
   *   TRUE if the file was created.
   */
  public function createJsFile(
    string $machineName,
    string $provider,
    ?string $componentType,
  ): bool {
    $file = fopen(
      $this->buildComponentFolderPath($machineName, $provider, $componentType) . "/$machineName.js",
      'x'
    );

    if (!$file) {
      return FALSE;
    }

    fwrite($file, "(function (Drupal) {\n");
    fwrite($file, "  Drupal.behaviors.$machineName = {\n");
    fwrite($file, "    attach: function attach(context) {\n");
    fwrite($file, "      console.log('$machineName JS');\n");
    fwrite($file, "    }\n");
    fwrite($file, "  };\n");
    fwrite($file, "})(Drupal);\n");
    return fclose($file);
  }

  /**
   * Create the component css file.
   *
   * @param string $machineName
   *   The component machine name.
   * @param string $provider
   *   The component provider.
   * @param string|null $componentType
   *   The component type.
   *
   * @return bool
   *   TRUE if the file was created.
   */
  public function createCssFile(
    string $machineName,
    string $provider,
    ?string $componentType,
  ): bool {
    $files = [];
    $config = $this->configFactory->get(ConfigureComponentsTypesForm::CONFIG_NAME);
    $extensions = ['.css'];

    if ($config->get('css_extension') && $config->get('css_extension') !== '.css') {
      $extensions[] = $config->get('css_extension');
    }

    foreach ($extensions as $extension) {
      $file = fopen(
        $this->buildComponentFolderPath($machineName, $provider, $componentType) . "/$machineName$extension",
        'x'
      );
      $files[$extension] = $file;

      if (!$file) {
        continue;
      }

      fwrite($file, "/*\n");
      fwrite($file, " * $machineName CSS\n");
      fwrite($file, " */\n");
      $files[$extension] = fclose($file);
    }

    return !in_array(FALSE, $files, TRUE);
  }

  /**
   * Load the component yaml file.
   *
   * @param \Drupal\Core\Plugin\Component $component
   *   The component.
   *
   * @return array
   *   The component yaml file.
   */
  public function loadComponentFile(Component $component): array {
    $pluginDefinition = $component->getPluginDefinition();
    $filePath = $pluginDefinition['_discovered_file_path'];

    if (!file_exists($filePath)) {
      return [];
    }

    // Load YAML file.
    $yaml = file_get_contents($filePath);
    // Decode YAML file.
    $yaml = Yaml::decode($yaml);
    return $yaml;
  }

  /**
   * Save the component yaml file.
   *
   * @param \Drupal\Core\Plugin\Component $component
   *   The component.
   * @param array $yamlArray
   *   The component yaml file.
   *
   * @return bool
   *   TRUE if the file was saved.
   */
  public function saveComponentFile(Component $component, array $yamlArray): bool {
    $pluginDefinition = $component->getPluginDefinition();
    $filePath = $pluginDefinition['_discovered_file_path'];
    $yaml = Yaml::encode($yamlArray);
    file_put_contents($filePath, $yaml);

    $this->messenger->addMessage($this->t('File @file has been saved successfully', ['@file' => $filePath]));
    return TRUE;
  }

  /**
   * Delete the component file.
   *
   * @param \Drupal\Core\Plugin\Component $component
   *   The component.
   * @param string $fileType
   *   The file type.
   *
   * @return bool
   *   TRUE if the file was deleted.
   */
  public function deleteComponentFile(Component $component, string $fileType): bool {
    $pluginDefinition = $component->getPluginDefinition();
    $path = $pluginDefinition['path'];
    $machineName = $pluginDefinition['machineName'];
    $filePath = "$path/$machineName.$fileType";

    if (file_exists($filePath)) {
      unlink($filePath);
      $this->messenger->addMessage($this->t('File @file has been removed successfully', ['@file' => $filePath]));
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Duplicate the component files and folder.
   *
   * @param \Drupal\Core\Plugin\Component $component
   *   The component.
   * @param string $newProvider
   *   The new component provider.
   *
   * @return bool
   *   TRUE if the folder was duplicated.
   */
  public function duplicateComponent(Component $component, string $newProvider): bool {
    $metadata = $component->metadata;
    $group = $metadata->group;
    $machineName = $metadata->machineName;
    $path = $metadata->path;
    $newPath = $this->buildComponentFolderPath($machineName, $newProvider, $group);

    if ($newPath === $path) {
      return FALSE;
    }

    if (file_exists($newPath)) {
      $this->messenger->addError($this->t('Folder @folder already exists', ['@folder' => $newPath]));
      return FALSE;
    }

    // Build the realpath of the new folder.
    $groupPath = dirname($newPath);
    $newPath = (realpath($groupPath) ?: $groupPath) . '/' . basename($newPath);

    $this->fileSystem->prepareDirectory($newPath, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);

    // Copy the files from old folder to new folder.
    $dir = new \RecursiveDirectoryIterator($path, \FilesystemIterator::SKIP_DOTS);
    $iterator = new \RecursiveIteratorIterator($dir, \RecursiveIteratorIterator::LEAVES_ONLY);

    foreach ($iterator as $file) {
      $filepath = $file->getPathname();
      $newFilePath = str_replace($path, $newPath, $filepath);

      if (is_dir($file)) {
        $this->fileSystem->prepareDirectory($newFilePath, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
      }
      else {
        $this->fileSystem->copy($file, $newFilePath);
      }

      // If files is component.yml, add replaces: from:base_component.
      if (str_ends_with($file, 'component.yml')) {
        $yaml = Yaml::decode(file_get_contents($filepath));
        $yaml['replaces'] = $component->getPluginId();
        file_put_contents($newFilePath, Yaml::encode($yaml));
      }
    }

    $this->messenger->addMessage($this->t(
      'Folder @folder has been duplicated successfully to @new_folder',
      [
        '@folder' => $path,
        '@new_folder' => $newPath,
      ]
    ));

    return TRUE;
  }

  /**
   * Move the component files and folder.
   *
   * @param \Drupal\Core\Plugin\Component $component
   *   The component.
   * @param string $newMachineName
   *   The new component machine name.
   * @param string $newProvider
   *   The new component provider.
   * @param string|null $newComponentType
   *   The new component type.
   *
   * @return bool
   *   TRUE if the folder was moved.
   */
  public function moveComponentFolder(
    Component $component,
    string $newMachineName,
    string $newProvider,
    ?string $newComponentType = NULL,
  ): bool {
    $metadata = $component->metadata;
    $machineName = $metadata->machineName;
    $path = $metadata->path;
    $newPath = $this->buildComponentFolderPath($newMachineName, $newProvider, $newComponentType);

    if ($newPath === $path) {
      return FALSE;
    }

    if (file_exists($newPath)) {
      $this->messenger->addError($this->t('Folder @folder already exists', ['@folder' => $newPath]));
      return FALSE;
    }

    $extensions = ['twig', 'js', 'css', 'component.yml', 'README.md'];

    foreach ($extensions as $extension) {
      if (!file_exists("$path/{$machineName}.$extension") || $machineName === $newMachineName) {
        continue;
      }
      $this->fileSystem->move("$path/{$machineName}.$extension", "$path/$newMachineName.$extension");
    }

    $newPathDirectory = dirname($newPath);
    $this->fileSystem->prepareDirectory($newPathDirectory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $this->fileSystem->move($path, $newPath);

    $this->messenger->addMessage($this->t(
      'Folder @folder has been moved successfully to @new_folder',
      [
        '@folder' => $path,
        '@new_folder' => $newPath,
      ]
    ));

    return TRUE;
  }

  /**
   * Get the provider path.
   *
   * Depending on the provider, get the path of the theme or module.
   *
   * @param string $provider
   *   The component provider.
   *
   * @return string
   *   The theme or module path.
   */
  private function getProviderPath(string $provider): string {
    $themeOrModulePath = $provider;

    if ($this->moduleExtensionList->exists($provider)) {
      $themeOrModulePath = $this->moduleExtensionList->getPath($provider);
    }

    if ($this->themeExtensionList->exists($provider)) {
      $themeOrModulePath = $this->themeExtensionList->getPath($provider);
    }

    return $themeOrModulePath;
  }

  /**
   * Flatten the library files.
   *
   * @param array $libraryYaml
   *   The component library yaml.
   * @param string $basePath
   *   The base path.
   * @param array $flattenedLibraries
   *   The component libraries files.
   *
   * @return array
   *   The flattened libraries files.
   */
  private function flattenLibraryFiles(
    array $libraryYaml,
    string $basePath,
    array &$flattenedLibraries,
  ): array {
    foreach ($libraryYaml as $libraryKey => $libraryValues) {
      if (!is_array($libraryValues)) {
        continue;
      }

      if (count($libraryValues) > 0) {
        $this->flattenLibraryFiles($libraryValues, $basePath, $flattenedLibraries);
      }

      $extension = pathinfo($libraryKey, PATHINFO_EXTENSION);

      if ($extension !== 'css' && $extension !== 'js') {
        continue;
      }

      $flattenedLibraries[$extension][] = "$basePath/$libraryKey";
    }

    return $flattenedLibraries;
  }

  /**
   * Get the provider's component files.
   *
   * @param string $providerPath
   *   The component provider path.
   * @param array $flattenedLibraries
   *   The component libraries files if given.
   *
   * @return array
   *   The flattened libraries files.
   */
  private function getProviderComponentsFiles(string $providerPath, array $flattenedLibraries): array {
    $componentsPath = "$providerPath/components";

    if (!file_exists($componentsPath)) {
      return $flattenedLibraries;
    }

    $dir = new \RecursiveDirectoryIterator($componentsPath, \FilesystemIterator::SKIP_DOTS);
    $iterator = new \RecursiveIteratorIterator($dir, \RecursiveIteratorIterator::LEAVES_ONLY);

    foreach ($iterator as $file) {
      $filepath = $file->getPathname();
      $extension = pathinfo($filepath, PATHINFO_EXTENSION);

      if ($extension !== 'css' && $extension !== 'js') {
        continue;
      }

      $flattenedLibraries[$extension][] = $filepath;
    }

    return $flattenedLibraries;
  }

  /**
   * Get the parent theme libraries.
   *
   * @param string $provider
   *   The component provider.
   * @param string $providerPath
   *   The component provider path.
   * @param array $flattenedLibraries
   *   The component libraries files.
   *
   * @return array
   *   The component libraries files.
   */
  private function getParentThemeLibraries(string $provider, string $providerPath, array $flattenedLibraries): array {
    if (!$this->themeExtensionList->exists($provider)) {
      return $flattenedLibraries;
    }

    $infoYmlFile = "$providerPath/$provider.info.yml";
    $infoYml = file_exists($infoYmlFile) ? Yaml::decode(file_get_contents($infoYmlFile)) : [];

    // No base theme then no need to get the parent theme libraries.
    if (!array_key_exists('base theme', $infoYml)) {
      return $flattenedLibraries;
    }

    // Get the base theme libraries.
    $baseTheme = $infoYml['base theme'];
    $baseThemeLibraryFiles = $this->getLibrariesFilesFromExtension($baseTheme, $flattenedLibraries);
    $flattenedLibraries = array_merge_recursive($flattenedLibraries, $baseThemeLibraryFiles);

    return $flattenedLibraries;
  }

  /**
   * Get the provider's libraries files.
   *
   * @param string $provider
   *   The component provider.
   * @param array $flattenedLibraries
   *   The component libraries files if given.
   *
   * @return array
   *   The component libraries files.
   */
  public function getLibrariesFilesFromExtension(
    string $provider,
    array $flattenedLibraries = ['js' => [] , 'css' => []],
  ): array {
    $providerPath = $this->getProviderPath($provider);
    // Get the parents files (css, js).
    $flattenedLibraries = $this->getParentThemeLibraries($provider, $providerPath, $flattenedLibraries);

    // Get all the provider files set in the libraries.yml file.
    $libraryFile = "$providerPath/$provider.libraries.yml";

    if (file_exists($libraryFile)) {
      $library = Yaml::decode(file_get_contents($libraryFile));
      $flattenedLibraries = $this->flattenLibraryFiles($library, $providerPath, $flattenedLibraries);
    }

    // Get all the components files (css, js) from the provider.
    $flattenedLibraries = $this->getProviderComponentsFiles($providerPath, $flattenedLibraries);

    return $flattenedLibraries;
  }

}
